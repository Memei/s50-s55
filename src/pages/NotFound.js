import Banner from '../components/Banner';

export default function NotFound() {

	let bannerNotFound = {
		title: "Error 404",
		description: "Page Not Found",
		buttonAction: "Go back to homepage",
		linkRoute: "/"
	}

	return (
		<Banner bannerProp={bannerNotFound}/>
	)
}