import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	// Checkss to see if the data was successfully passed
	// console.log(props);
	// Every cmponent receives info in a form of an object
	// console.log(typeof props);
	// console.log({courseProp});

	const { name, description, price, _id } = courseProp

	/*
		Use the state hook for this component to be able to store its state.
		States are used to keep track of the info related to individual components/elements

		Syntax:
			const [getter, setter] = useState(initialGetterValue);
	*/
	// State hook to store the state of enrollees
	const [ count, setCount] = useState(0);
	const [ seats, setSeats ] = useState(10)
	// console.log(useState(0))

/*	function enroll() {
		if(count < 30) {
			setCount(count + 1)
			setSeats(seats - 1)
			console.log('Enrollees ' + count)
			console.log('Seats ' + seats)	
		} else {
			alert("No more seats!")
		}
	}*/

	function enroll() {
		if(seats > 0) {
			setCount(count + 1)
			console.log('Enrollees ' + count)
			setSeats(seats - 1)
			console.log('Seats ' + seats)	
		} /*else {
			alert("No more seats available!")
		}*/
	};

	useEffect(() => {
		if (seats === 0) {
			alert("No more seats available.")
		}
	}, [seats])

	return (
		<Row className="my-3">
			<Col xs={12} md={6}>
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
				    <Card.Text>{description}</Card.Text>
				    <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
				    <Card.Text>{price}</Card.Text>
	       		 	<Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>

	)
}

